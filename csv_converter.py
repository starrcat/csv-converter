#!/usr/bin/env python

from __future__ import with_statement
from PIL import Image
from os import walk

PICS_DIR = "./pics/"

fnames = []
for (dirpath, dirnames, filenames) in walk(PICS_DIR):
    fnames.extend(filenames)
    break


def make_csv(filename):
    filer = PICS_DIR + filename
    print(filename)
    #pos = filename.lower().find(".png")
    #label = filename[pos-1:pos] # Save label of this digit
    im = Image.open(filer)  # relative path to file
    im = im.convert("L")
    pix = im.load()
    width, height = im.size

    with open('./output_file.csv', 'a') as f:
        for y in range(width):
            for x in range(height):
                r = 255-pix[x, y]
                if (x == width - 1) and (y == height - 1):
                    f.write('{0}'.format(r))
                else:
                    f.write('{0},'.format(r))
        f.write('\n')
for item in fnames:
    make_csv(item)
